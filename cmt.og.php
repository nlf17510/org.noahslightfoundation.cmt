<?php

/**
 * The oGroup class provides handling for managing option
 * groups and their values of this extension.
 */
class CRM_Cmt_oGroup {

    public $oGroup;
    private $ogDetails = array();
    public $ogValues = array();

    public function __construct($optionGroupName, $getValuesToo = FALSE) {
        // ideal optionGroup Details
        $this->ogDetails = array(
            'name' => $optionGroupName,
            'title' => "Entities and their Assigned Message Templates",
            'description' => "This group holds user-defined assignments of templates",
            'is_active' => 1,
            'is_reserved' => 0,
            'data_type' => "Integer",
        );
        $this->getoGroupFromDB();
        if ($getValuesToo) $this->getoGroupValues();
    }

    public function getoGroupFromDB() {
        try {
            $optionGroup = civicrm_api3('OptionGroup', 'get', array(
                'sequential' => 1,
                'name' => $this->ogDetails['name'],
            ));
            $this->oGroup = $optionGroup['values'];
        } catch (\Exception $e) {
            throw new Exception($e->__toString());
            return NULL;
        }
    }

    public function createoGroupInDB() {
        try {
            $result = civicrm_api3('OptionGroup', 'create', $this->ogDetails);
            if ($result['count'] < 1) throw new Exception('Unable to create and save Option Group');
            $this->getoGroupFromDB();
        } catch (\Exception $e) {
            throw new Exception($e->__toString());
            return FALSE;
        }
    }

    public function returnGroupValues() {
        if (empty($this->ogValues)) return NULL;
        return $this->ogValues;
    }

    public function getoGroupValues() {
        try {
            $result = civicrm_api3('OptionValue', 'get', array(
                'option_group_id' => $this->ogDetails['name'],
            ));
            $this->ogValues = $result['values'];
        } catch (\Exception $e) {
            throw new Exception($e->__toString());
            $this->ogValues = NULL;
        }
    }

    public function getOptionValueBy($key, $value) {
//        as of 4.7.25, an option value looks like
//         {
//            "id": "879",
//            "option_group_id": "99",
//            "label": "Contribution Page 1",
//            "value": "66",
//            "name": "contrib_1",
//            "filter": "0",
//            "is_default": "0",
//            "weight": "1",
//            "is_optgroup": "0",
//            "is_reserved": "0",
//            "is_active": "1"
//        }
//        if (empty($this->ogValues)) throw new Exception('You must populate ogValues before running this method.');
        foreach ($this->ogValues as $id => $array) {
            if ($array[$key] == $value) return $array;
        }
        return NULL;
    }

    public function deleteOptionValue($id) {
        try {
            $result = civicrm_api3('OptionValue', 'delete', array(
                'sequential' => 1,
                'id' => $id,
                'options' => array('limit' => 1),
            ));
        } catch (\Exception $e) {
           throw new Exception($e->__toString());
           return FALSE;
        }
    }

    public function createOptionValue($user_label, $name, $value) {
        try {
            $result = civicrm_api3('OptionValue', 'create', array(
                'sequential' => 1,
                'option_group_id' => $this->ogDetails['name'],
                'label' => $user_label,
                'name' => $name,
                'value' => $value,
            ));
        } catch (\Exception $e) {
            throw new Exception($e->__toString());
            return FALSE;
        }
    }
}