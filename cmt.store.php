<?php

/**
 * The oStore class lists the entities that can be manipulated
 * by this extension. Ex: if wanting to manage an Event form, enter the
 * Event entity to the master list below and its form.
 */
class CRM_Cmt_oStore {

    public static function masterList() {
        return array(
            'Contribute' =>
                array(
                    'name' => 'Contribute',
                    'prefix' => 'contrib_',
                    'forms' => array('CRM_Contribute_Form_ContributionPage_ThankYou' => '.crm-contribution-contributionpage-thankyou-form-block .crm-submit-buttons'),
                    'mailParams' => array('contribution_online_receipt'),
                ),
            'Event' =>
                array(
                    'name' => 'Event',
                    'prefix' => 'event_',
                    'forms' => array('CRM_Event_Form_ManageEvent_Registration' => '#registration_blocks #mail legend'),
                    'mailParams' => array('event_online_receipt'),
                ),
        );
    }

    public static function returnEntityProperties($entity) {
        $allEntities = self::masterList();
        if (array_key_exists($entity,$allEntities)) return $allEntities[$entity];
        return FALSE;
    }

    public static function formNameInEntity($formName) {
        // the hook buildForm doesnt provide the entity it's dealing with, only the form and formName BUMMER
        foreach (self::masterList() as $allEntities => $entity) {
            foreach ($entity['forms'] as $form => $jsPlacement) {
                if ($formName == $form) return $entity;
            }
        }
        return FALSE;
    }
}