<?php

require_once 'cmt.civix.php';
use CRM_Cmt_ExtensionUtil as E;

/*
 * Custom handling of option group and values for this extension
 */
require_once 'cmt.og.php';
use CRM_Cmt_oGroup as oGroup;

/*
 * Utility interface
 */
require_once 'cmt.entity.php';
use CRM_Cmt_oEntity as oEntity;

/*
 * Utility interface
 */
require_once 'cmt.utility.php';
use CRM_Cmt_oUtility as oUtility;

/*
 * Utility interface
 */
require_once 'cmt.store.php';
use CRM_Cmt_oStore as oStore;


/**
 * Implements hook_civicrm_config().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_config
 */
function cmt_civicrm_config(&$config) {
  _cmt_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_xmlMenu().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_xmlMenu
 */
function cmt_civicrm_xmlMenu(&$files) {
  _cmt_civix_civicrm_xmlMenu($files);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_install
 */
function cmt_civicrm_install() {
  _cmt_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_postInstall().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_postInstall
 */
function cmt_civicrm_postInstall() {
  _cmt_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_uninstall
 */
function cmt_civicrm_uninstall() {
  _cmt_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_enable
 */
function cmt_civicrm_enable() {
    try {
        $cmt_in_db = new oGroup(E::LONG_NAME);
        if (empty($cmt_in_db->oGroup)) {
            $cmt_in_db->createoGroupInDB();
        }
    } catch (\Exception $e) {
        CRM_Core_Error::debug_log_message($e->__toString(),FALSE,E::LONG_NAME);
        $session = CRM_Core_Session::singleton();
        $session->setStatus("While enabling the extension, there was an error. Please contact your system administrator for assistance.", 'Error', 'error', array('expires'=>0));
    } finally {
        unset($cmt_in_db);
    }
  _cmt_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_disable
 */
function cmt_civicrm_disable() {
  _cmt_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_upgrade
 */
function cmt_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _cmt_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_managed().
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_managed
 */
function cmt_civicrm_managed(&$entities) {
  _cmt_civix_civicrm_managed($entities);
}

/**
 * Implements hook_civicrm_caseTypes().
 *
 * Generate a list of case-types.
 *
 * Note: This hook only runs in CiviCRM 4.4+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_caseTypes
 */
function cmt_civicrm_caseTypes(&$caseTypes) {
  _cmt_civix_civicrm_caseTypes($caseTypes);
}

/**
 * Implements hook_civicrm_angularModules().
 *
 * Generate a list of Angular modules.
 *
 * Note: This hook only runs in CiviCRM 4.5+. It may
 * use features only available in v4.6+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_angularModules
 */
function cmt_civicrm_angularModules(&$angularModules) {
  _cmt_civix_civicrm_angularModules($angularModules);
}

/**
 * Implements hook_civicrm_alterSettingsFolders().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_alterSettingsFolders
 */
function cmt_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _cmt_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

// --- Functions below this ship commented out. Uncomment as required. ---

/**
 * Implements hook_civicrm_preProcess().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_preProcess
 *
function cmt_civicrm_preProcess($formName, &$form) {

} // */

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_navigationMenu
 *
function cmt_civicrm_navigationMenu(&$menu) {
  _cmt_civix_insert_navigation_menu($menu, NULL, array(
    'label' => E::ts('The Page'),
    'name' => 'the_page',
    'url' => 'civicrm/the-page',
    'permission' => 'access CiviReport,access CiviContribute',
    'operator' => 'OR',
    'separator' => 0,
  ));
  _cmt_civix_navigationMenu($menu);
} // */

/**
 *  Add the dropdown to the QuickForm page
 * @param $formName
 * @param $form
 */
function cmt_civicrm_buildForm($formName, &$form) {

    try {
        // for reference
        $formID = $form->getVar('_id');

        // checks if formName can be managed by this extension
        if (!$entity = oStore::formNameInEntity($formName)) return;

        // if so, lets save this entity for future reference
        $entity = new oEntity($entity);
        $entity->setForm($formName);

        // exit early if no user-made message templates
        $allTemplates = oUtility::getUserTemplates();
        if (!$allTemplates) {
            $session = CRM_Core_Session::singleton();
            $session->setStatus("The extension Custom Message Templates is enabled but there exists only system message templates. Please go to or ask your CiviCRM administrator for assistance.", 'Note', 'alert', array('expires' => 0));
            return;
        }

        // format the api results for the quickform dropdown
        $dropdownOptions = array();
        foreach ($allTemplates as $template_id => $templateName) {
            $dropdownOptions[$template_id] = $templateName['msg_title'];
        }

        // check if template is already assigned
        $cmt_oGroup = new oGroup(E::LONG_NAME, TRUE);
        $name = $entity->getPrefix() . $formID;
        $optionName = $cmt_oGroup->getOptionValueBy('name', $name);

        // if template is already assigned to this form, show it as the default value for the html-element
        $placeholder_id = !empty($optionName) ? $allTemplates[$optionName['value']] : NULL;

        // create form element
        $form->setDefaults(array('messageTemplates' => $placeholder_id));
        $form->add('select',
            'messageTemplates',
            ts('Available Message Templates'),
            array('' => ts('- default -')) + $dropdownOptions,
            FALSE,
            array('class' => 'crm-select2', 'style' => 'width: 500px')
        );

        // get our custom dropdown from a template built into our extension
        $templatefile = E::path('templates/messageTemplateSelector.tpl');

        // inject our html-element-template into CiviCRM
        CRM_Core_Region::instance('form-top')->add(array(
            'template' => $templatefile
        ));

        // using js, where do we move our dropdown within the form?
        $jsPlacement = $entity->getJsLocation($formName);
        $form->assign('jsPlacement', $jsPlacement);

    } catch (\Exception $e) {
        CRM_Core_Error::debug_log_message($e->__toString(),FALSE,E::LONG_NAME);
        $session = CRM_Core_Session::singleton();
        $session->setStatus("The extension Custom Message Templates is enabled but was unable to build a dropdown. Please see your system administrator for assistance.", 'Note', 'alert', array('expires' => 0));
    }

}

function cmt_civicrm_postProcess($formName, &$form) {

    try {
        // for reference
        $formID = $form->getVar('_id');

        // checks if formName can be managed by this extension
        if (!$entity = oStore::formNameInEntity($formName)) return;

        // if so, lets save this form's entity-type (contrib, event etc) for future reference
        $entity = new oEntity($entity);

        // extension's dropdown can be NULL or a user-specified template
        $desiredTemplateID = CRM_Utils_Array::value('messageTemplates', $form->_submitValues);

        // prepare our optionGroup
        $oGroup = new oGroup(E::LONG_NAME, TRUE);

        // prepare our optionValue name
        $name = $entity->prefix . $formID;

        // process the dropdown's selected value
        if (empty($desiredTemplateID)) {
            // check db, and if a custom template has been set for this form, unset the custom template association
            // scenario: user is changing from previously specified custom template to NULL
            if (!empty($setting = $oGroup->getOptionValueBy('name', $name))) {
                //unset it!
                $oGroup->deleteOptionValue($setting['id']);
            }
        } else {
            // user has specified a template

            // if an optionValue already exists, delete it
            if (!empty($setting = $oGroup->getOptionValueBy('name', $name))) {
                $oGroup->deleteOptionValue($setting['id']);
            }

            $user_label = $entity->getName() . ' Page ' . $formID;
            $oGroup->createOptionValue($user_label, $name, $desiredTemplateID);
        }
    } catch (\Exception $e) {
        CRM_Core_Error::debug_log_message($e->__toString(),FALSE,E::LONG_NAME);
        $session = CRM_Core_Session::singleton();
        $session->setStatus("The extension Custom Message Templates was unable to save your preference for a custom template.", 'Note', 'alert', array('expires' => 0));
    }
}

function cmt_civicrm_alterMailParams(&$params, $context) {

    if ($context != 'messageTemplate') return;

    $oGroup = new oGroup(E::LONG_NAME, TRUE);
    $oValues = $oGroup->returnGroupValues();

    // figure out better strategy to move these into separate class
    // I dont like the prefix(es) hardcoded here
    if (array_key_exists('event', $params['tplParams'])) {
        $user_defined = $oGroup->getOptionValueBy('name', 'event_'.$params['tplParams']['event']['id']);
    }
    if (array_key_exists('contribute',$params['tplParams'])) {
        $user_defined = $oGroup->getOptionValueBy('name', 'contrib_'.$params['tplParams']['contribute']['id']);
    }

    if (empty($user_defined)) return;

    $params['messageTemplateID'] = $user_defined['value'];
}
