![Alt text](https://img.shields.io/badge/Development%20Stage-alpha-yellowgreen.svg?raw=true "Development Stage") ![Alt text](https://img.shields.io/badge/Test%20Coverage-0%25-lightgrey.svg?raw=true "Test Coverage") ![Alt text](https://img.shields.io/badge/Code%20quality-Needs%20assistance-blue.svg?raw=true "Code Quality")

#Custom Message Templates
`<coded>` by Noah's Light Foundation of Winter Garden, FL, USA

##Purpose
This extension allows a user to override message templates that CiviCRM automatically sends to constituents when making a contribution etc.

###Instructions
1. Create a custom message template via **CiviCRM > Administer > Communications > Message Templates** and save.
2. Go to the **Receipt tab** of any Contribution Page and select your custom message template there.

![Alt text](./cmt_feature.png?raw=true "Optional Title")

##User Settings
Handled via Option Group
    - Create
    - Read
    - Update
    - Delete

###Questions / Doubts / Comments
**Q: Why is the optionValue name `contrib_1` as opposed to `CRM_Contribute_Form_ContributionPage_ThankYou?`**  

Answer: `contrib_1 == Contribution Page 1  // can have its own message template`  
and `contrib_2 == Contribution Page 2  // can have its own message template`